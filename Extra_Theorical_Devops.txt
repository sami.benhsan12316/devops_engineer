1. Docker Vs Virtual Machine : 
	1.1:  Composants d'un Système d'exploitation : 
	Kol Os métkawén mn 3 layers ykhdmo fou9 b3a4hom : 
		- OS kernel : howa 9alb OS li ycommuniki m3a Couche Hardware ta3 pc  mté3k .. driver etc ....
		- Applications : li tcommuniki m3a OS kernel 
		RQ : Par Exemple 3andk Ubuntu w mint w debian etc .. des versions fl linux kol ya3nhom nafs noyau li 
		howa Unix ama couche application differente binét b3a4hom ..... 
	1.2:  Docker vs VM : 
	Docker w les VM zouz des outils de virtualisations difference binéthom ennou docker yvirtualisi kén couche 
	application  w yésta3ml OS kernel mté3k  w VM tvirtualisi les 2 couches Application + Os Kernel ( 3ando OS kernel mté3o ) . Par conséquent  : 
		- Volume : Docker image dima a5af ml VM . Docker des mégabytes ..... Vms des Gygabytes ...... 
		- Vitesse : Docker Asra3 .. yraka7 juste couche application ..
		- Compatibilité : VM compatible m3a ay OS 3aks Docker ( bch nzido nrafiniw akthr fazét compatibilité ) 
			( En réalité Docker images  mch à 100 / 100 compatible m3a ay OS kima 9olna 
			ki tinstalli image docker à base de linux 3la OS windows < Windows 10 wili les versions Mac 9dom 
			ysir un pb de compatibilité m3a Noyau windows ( héka 3léh 9bal matji tinstalli Docker fl windows 
			lézmk t4abét est ce que OS mté3k ysupporti Docker native wla si nn lézmk tsob outil esmo Docker Toolbox
			li ya3ml abstraction ll OS kernel w y5alih capable bch ysupporti docker images ) 

2. Terraform : 
1 : Terraform Architecture : ( Voir Capture Terraform Architecture  + Zidha li fama Provider IaaS ba3d Core kima AWS , Azure , etc...) 
	- TF Config : Fichier .tf de configuration yéktbo developper  ( Desired State ) 
	- State : Etat Actuel mta3 Infra té3k ( Current State ) 
	- Core : Yé5ou TF Config ( Desired State )  + State ( Current State ) -> w yfaxi un plan d'éxécution ( Desired - Actuel ) ( chnowa lézmo yzid / yna9as / yupdati fl infra etc ..) 
w ylanci plan d'éxécution hétha 
	- Provider [IaaS : Infrastructure As A Service ] : Ycomminiki Terraform m3a Provider bch ya3tih lplan w ylancih  fi wosto -> Tétsan3 fih Infra ( Soura ta9ribia
léli ysir ) 
RQ : Terraform howa responsable 3la énnou ycomminiki m3a Provider w yéfhm API mté3o w y5dm aliha.. énti CONCRAITEMENT juste téktb les .tf de configuration 
+ tlanci les commands ( 5 ) 

RQ 1: 
Declarative Language : juste énti tfaxi résultut final li t7éb 3lih w langage yousélo 
Impérative Language : Enti Lézmk tfaxi kol étape chlézm ysir fiha .. tasna3 un plan d'instructions
RQ 2: 
Avantage fi énou Terraform Declarative Language .. Enti juste demandé énk ta3tih configuration final w tokhroj mnha .. w howa yousélha .. ( t9olo nhb par exemple
7 serveurs w 2 vpc etc ... w howa ygénéri plan difference bin infra actuel & infra désirée w ba3d yapplikih bch yousélha  ) .. 
Par contre kén ja Impérative Language énti li lézmk ta3tih chnowa ya3ml bch yousel ll config final .. ( zid 2 serveurs .. na9as vpc.. etc .. ta3tih un plan d'instruction ) 



26/ BLABLABLA 1 : 
Ki bch tji tsetti K8s Cluster fama haja  lézm configuriha bch tkoun capable bch tésta9bl requests externes li hia : LoadBalancer w elle dépend de : 
	- Environnement li 9é3éd ydour fih K8s Cluster : 
		1 : Kenk t5dm 3la environnement cloud : (Voir capture Cloud Load Balancer ) 
	généralement bch ykoun 3andk Cloud Load Balancer implémenté ml fournisseur howa il reçoit les requests w yforwardihom ll K8s Cluster 
	==> Avantage majeur : Load Balancer Déja implémenté 
		2 : Kénk t5dm 3la serveur 3adi 3andk : Lézmk tconfiguri Entry point todkhol mnha les requests 
		Fama plusieurs manières pr faire ça ... ( Mawaratch kifh ) 
	==> Plus généralement dima lézmna entry point (LoadBalancer)  todkhol menha les requests .. par exemple External proxy server li ynajém ykoun soit software 
soit hardware entrypoint todkhol alih requestét w yforwardiha ll k8s cluster en effet : 
	- tékho sérveur séparé .. ta3tih @ip w t7éll port 3lih yé9bl 3lih requestét w haka wala howa entry point ta3 cluster w howa wa7id accesible ml bara .. 
wala composant ékhr ywali accessible ml bara ( Voir capture proxy server ) -> Good security practice
	- Request tji ml bara ll proxy server yforwardiha ll Ingress Controller pod ythabét rules li fl ingress w ychouf anéhi li bch yapplikiha w ylansi forwarding 
	CC : Tout dépend de l'environnement .. lféyda lézmk tconfiguri entrypoint todkhol aliha requestét w tforwardiha ll k8s cluster
	#Question 23 : LoadBalancer Service vs LoadBalancer hétha .. 
	#Question 24 : ékhi tw fl lkher request 3la chkoun bch todkhol .. ingress , Ingress CP , LB ? 
	#Question 25 : C'est très théorique !! .. 


x :BLABLABLA 2: 
	22.2.1.1: Kiféh Pod yé5ou @ip ?  
	* pod fih 2 containers mé7chi wost node  .. bch yé5ou @ip ( # kubectl get pod -o wide tél9aaha ) ta7t range des @ ta3 node ( node 3ando range ta3 adress wost cluster 
	10.1.2.x par exemple x numéro pod t5ayélha ( en négligeant les détails de 8 bits etc .. -> tjm tfhm 3léh @ip des pods tétbadél ki yti7 pod yji lékhr .. kol mara x tétbadél 
	ki ymout pod w yji wéhéd ékhr ) 
	# On considère 2 replicas du pod .. donc on a 2 nodes 3lihom 2 pods w fkol pod 2 containers
	RQ : Les replicas 3andhom nafs métadata 
				22.2.1.2 : Kiféh service yforwardi ll pod w containers mté3o ? 
	* service yji 9odém podét avec une @ip & port fixes yé9bl request jéya ml ingress li howa bido wé5ouha ml bara 
	kifh service ya3rf l ana : 
		- pod yforwardi : bl selector .. yselecti label ta3 deployement des pods  ( li howa généralement nafsou label ta3 les pods ) 
		# Tba3wila 1: jarab mathotch nafs ésm label bin deployement w pod w chouf service bchkoun lézm yétrbat bl label ta3 deployement wili pod wili zouz kifkif 
		RQ : les replicas kol 3andhom nafs label -> une fois forwardina l pod yforwardi l b9iyét replicasét
		- port yforwardi : bl targetPort 
				22.2.1.3 : Endpoint object : 
	* Endpoint object ( voir capture endpoint object ) : yétsna3 wa7dou automatiquement .. tal9a fih les pods ( les containers wost pods plus précisament ) associé au service 
	.. kubectl get endpoints .. endpoint yokhroj ss forme @ip_pod:targetPort .. mch @ip khw 
	* Elargir le contexte : de mm zéda request li woslot ll pod yforwardiha l service ékhr li howa bido yforwardi l podét mté3o etc ..
	# Tba3wila  2 : Kiféh pod yforwardi l service ? selector du pod yséléctionni label ta3 service comment faire ? hata fl démo kifh 3mélnéha URGENT !!!!  ( Résolu ) 
	-> Selector nésta3mlouh bch norbtou ( Service -> Deployement ) w ( Deployement -> Pods ) .. Ama bref , manajmouch nésta3mlouh bch norbtou Pod b service ( # Question 15 : Règle torbot biha 
	pod b service ? ) .. Exemple fl mongo-express rbatnéh b pod mongo ki 3adinélou variable d'environnement db_url fiha l'url ta3 base de données mongo 
	CC : manahkiwch m3a Pod toul .. lézmk tét3ada 3al service té3ou w howa yforwardi .. w généralement ykounou des service interne (CLUSTER-IP) .. yé9blo request w yforwardiwha ll podét
	li houma responsable alihom 

* 4 : Ansible ad_hoc commands : # Pas très utilisé .. 
	4.1 : Chnouma ? : 
	Houma des commands yosl7ou bch Ansible ynajém yconfiguri srévér li associé lilou (li configurithom fl Inventory)
	b des modules simples sans avoir besoin bch yésta3ml des playbooks 
	# Utilisation dans le cas ou nhb na3ml configuration sghira 3al srévér mte3i (module wéhéd) w nhbch nktb playbook 
	
	4.2 : Format : ansible [pattern] -m  [module] -a [options_du module] 
	# [pattern] : Tfaxi quel hosts/group de hosts / host wé7éd  li thb tconfigurih(om) 
	# [module] : Tfaxi command li thb tlansiha
	# [options_du module]  : Tfaxi les options li thb t3adihom ll command  
	
	4.3 : Exemple d'utilisation : Kiféh norbot ansible bl srévér mté3ou lkol li fil Inventory ? : 
	En applicant ad_hoc command héthi : ansible -i hosts -m ping # Voir capture ansible ping 
	# -i hosts : les serveurs lmoujoudin fl fichier hosts lkol 
	# ping : commande tconnecti ansible bl hosts mté3ou .. Question 

	4.4 : Autres Pattern : Kiféh nlanci ad_hoc command 3la group particulier de hosts : 
		ansible esmlgroup -i hosts -m ping # ywali ypingi kén 3al groupe de serveurs li inti 3adithoulou mch 3al les serveurs kol 
		6.4 : Kiféh nlanci ad_hoc command 3la serveur particulier fl hosts : 
		ansible @host_ip -i hosts -m ping # ywali ypingi kén 3al serveurs li inti 3aditlou @ip mté3ou mch 3al les serveurs kol 
	


* 6 : Modules : 
	6.1 : Idée : Fékrét les Modules énou fi 3ou4 ma tlansi des commandes manuellement houma yremplasiwhom w il 
	seront lancé fi wost des tasks w ces tasks se trouvent dans des playbooks 
	6.2 : Exemple : 
		Kima fl Démo Au lieu de : 
		-> apt install ngnix 3mélna 
		walina : 
		-> apt: 
				name: .....
				state: ... 
	RQ : Les Modules barcha , tal9ahom ma9soumin ss forme de groupes , w kol module 3ando : 
		- des paramètres w des valeurs najmou nda5louhom pour ces paramètres 
		- returned values # Question ? 
	hal chy hétha kol tél9ah fl doc : https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html


* 7 : Collections : # Mch mafhoum .. mlkher ... Question 
# Ma8ir yésr tba9bi9 .. Ansible ma3ach fih des modules séybin (ekhr wéhéd haka ansible 2.9) .. néss walét tkhdm bl Collections 
# A priori houma 3ibara 3la bakouet nhotou fihom kol chy (playbooks , modules , plugins etc .. )  
# Plugins : trouf ta3 code yétzédou m3a les modules bch ya3mlou extention de fonctionalités .. exp : module AWS yésna3/yfasakh instance .. plugin : yzidk 
option twali capable bch tfaltar quel VMs choisir ..

* 8. Ansible Galaxy : 
	- 3ibara 3la el repo git éli ma7touta fiha el code ta3 Collection  
	- ansible-galaxy collection install amazon.aws # bch tinstalli galaxy li fih collection d'aws  
	- tjm tupdati collection mn jémlét li habto m3a ansible bhal command d'installation 3ou4 ma tupgradi ansible kéml  
	- tnajém zéda tésna3 des collections 5asin bik fi cas de grands projets 
	- ness kol lézmha taba3 nafs Structure # Voir capture Ansible collection Structure 

1. Shell vs sh vs bash :  
	1.1: Shell : Enti ki tlansi command tét3ada 3la programme esmou shell .. yékhou command héthi ythabétha w ytarjémha l code yjm yfhmo kernel 
bch y3adih ll kernel li yexecuti commande héthi  
	1.2 : sh ( Bourne (Sh)ell ) : Naw3 mn anwé3 Shell , moujoud ss /bin/sh
	1.3 : bash ( (B)ourne (A)gain (S)hell ) : Version améliore de sh , moujoud ss /bin/bash ( Actuellement a8lab les systems 3andhom hétha ) 
->  GARDER FLKHER LI HIA MAHI ELLA PROGRAMMATION LINUX TKTBHA FI FICHIER EXTENTION .sh mahma kén el shell li tkhdm alih



3. Terraform vs Python (boto3) : 
# Terraform en gros khir éma fama hajét tnajém ta3mlhom kn bl boto3 ( kima if .. w programmation 3al infra en général ) 
# Plus de détails Ki t7a4ar entretien nchalh wili faza ...
