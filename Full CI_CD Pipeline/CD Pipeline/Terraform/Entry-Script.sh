#!/bin/bash 
# Installing , Starting , Adding Group to Docker 
sudo yum update -y && sudo yum install -y docker 
sudo systemctl start docker 
sudo usermod -aG docker ec2-user

# Installing Docker-compose 
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose