from kafka import KafkaConsumer
import json
print('Step 1 : Creating Consumer')

consumer = KafkaConsumer(
    client_id = 'Consumer' ,
    group_id = 'Group_1' ,
    bootstrap_servers = ['localhost:9092'] ,
    security_protocol = "SSL" ,
    value_deserializer = lambda value : json.loads(value.decode('utf-8') ),
    key_deserializer = lambda  key : json.loads(key.decode ('utf-8') ),
    api_version=(2,0,2)
)
print('Step 2 : N9aydoh fi Topic')
consumer.subscribe(topics = ['SBH'])
print(consumer.subscription())
print('Step 3 : Lecture de message')
for message in consumer :
    print("%d:%d: k=%s v=%s" % (message.partition,
                                message.offset,
                                message.key,
                                message.value))