PB 1 & 2 : hano chouf les 2 captures ( Résolu ) 
PB 3 : Kiféh nzid el custuom topic creation : ( Résolu ) 
if you want to customize any Kafka parameters, simply add them as environment variables in docker-compose.yml, e.g. in order to increase the message.max.bytes parameter set the environment to KAFKA_MESSAGE_MAX_BYTES: 2000000. To turn off automatic topic creation set KAFKA_AUTO_CREATE_TOPICS_ENABLE: 'false'
PB 4 : TypeError: Object of type bytes is not JSON serializable ( Résolu ) 
-> hia wa4a7 pb fl serialization , Ena wa9tha 8lot fl 
key:json.dumps(key).encode('utf-8') kont hatétha key:json.dumps(key.encode('utf-8')) Hano énti fakar 8ad fl zone héki .. 
PB 5 : consumer maya9rach fl events générés par le producer  (Résolu ) 
	solutions intuitives : 
	sol 1 : Vérifier si l'ordre de lancement bien vérifié ( chy ) 
	sol 2 : Vérifier si code né9s ( nn .. kol chy fait ) 
	solutions net : 
	sol 3 : bélkchi khatér group_id mch mséti ( chy ) 
	sol 4 : bélkchi khatér nsit producer.flush() (chy) 
	sol 5 : auto_offset_reset='earliest' (chy)
	sol 6 : fetch_max_wait_ms=0 (chy) 
	sol 7 : auto_commit_enable=True (chy) 
	sol 8 : L'ajout du nom du topic wa9t san3an producer OU consumer.subscribe(topics=['KHARJA']) résoulvent le pb .. 
	c'est bizarrre car malgré ki na7iwhom consumer nal9awah inscrit fih .. mch fibéli déja inscrit pas besoin de l'inscrire manuellement 
PB 6 : héki exec bl dollar takhtafch 
PB 7 : Dkhalt wost kafka_container w7élt ( La tba3wil fih najamto , la command li fl démo 3raft kifh tétlansa )  ( Résolu ) 
	- PB Command : 
	solutions intuitive : 
	- sol1 : chwaya tba3wil .. chy mchwa4a7 jémla w script mafamch 
	- sol2 : Image officiel mafihéch ml dékhl kifh
	- sol3 : youtube ( Oui Résolu : ce vidéo est héyél : https://www.youtube.com/watch?v=WnlX7w4lHvM  .. fih play with kafka 
	container .. w kiféh raka7 docker-compose file etc .. ) 
	




Remarques : 
1/ Kiféh Néjbd valeur aléatoire d'une liste 
import random
foo = ['a', 'b', 'c', 'd', 'e']
print(random.choice(foo)) 

Questions : 
1/ Wa9téli consumer yconnécti 3la broker ( bootstrap_servers = ['localhost:9092'] ) héka wa9tha automatiquement wala inscri fl 
topics li moujoudin fl broker héka lkol ? -> Bon à priori oui .. au moins haka fl pratique ..
2/ Pk 9092 .. : ki badélna port .. walét erreur : kafka.errors.NoBrokersAvailable: NoBrokersAvailable() .. Hna mch mafhom barcha
aléh .. en tt cas howa fl docker-compose héka li configuré ! 
3/ Apache Kafka Cluster .. Mn chno métkawén ékhi ? 
