Planning ChatGPT : 
    1/ Chnowa ELB ? (OK)
    2/ Types of ELB: 
        2.1 / Classic Load Balancer (CLB)
        2.2 / Application Load Balancer (ALB)
        2.3 / Network Load Balancer (NLB)
    3/ Target Groups
    4/ Listener Rules
    5/ Health Checks
    6/ SSL/TLS Termination
    7/ Security Groups and Access Control
    8/ Logging and Monitoring
    9/ Integration with Other AWS Services
    10/ Advanced Features





1. Chnowa ELB ?: (Kltmin khféf 3lih) : 
        - Elastic Load Balancing (ELB) : 3ando role d'un LB classique ama assisté bkolo mn AWS.. tjih charge de requests
        ywaza3ha b façon balancé 3ali wrah (9854548985 la9cha tnajém tkoun wrah enti w architecture li tkhdm fiha )
        - Fih 3 anwé3 (CLB,ALB,NLC) # En détails ba3d nchalah 
        - 3ando plusieurs fonctionalités kima : Health checks , SSL termination , Security ,Monitoring and logging etc ..
        # En détails ba3d nchalah 

9. Integration with Other AWS Services : (Ml fou9)

    1. Integration with EC2:
        - ELB tnajém tintégri wrah des EC2 yé9blo mno request li bch yforwardiha ml client kima haka : 
        
                                +----------+ 
                                | Client   |
                                +----------+
                                    | |
                                +-----------+
                                |    ELB    |
                                +-----------+
                                    | |
                        +---------+      +---------+
                        | EC2 1    | ... | EC2 n   |
                        +---------+      +---------+
        En effet , tjih request ml client w howa en se basant 3la algo de balancing & état ta3 instance yforwardi request l wahda howa ya5tarha 
        RQ : les EC2 msajlin fl ELB , kol mara ydour ytasti l'état mté3hom , el VM el tay7a ykharajha mn 7sébét forwarding  


    2. Integration with S3:
        - ELB tnajém tintégri wrah des S3 yé9blo mno request li bch yforwardiha ml client kima haka 
    
                                    +----------+ 
                                    | Client   |
                                    +----------+
                                        | |
                                    +-----------+
                                    |    ELB    |
                                    +-----------+
                                        | |
                            +---------+      +---------+
                            | S3 1    | ...  | S3 n    |
                            +---------+      +---------+
    
        RQ : Use case : Généralement na3mlou haka bch njéwbo 3al requests li talbin des fichier statiques (image,CSS,JS etc .. ) fi 3ou4 ma forward 
        ysir l des EC2 , na3mlouh pour des S3 (ywaliw houma responsable 3al les requests static) w haka nkhaféfo ml charge li 3al EC2  
    

    3. Integration with RDS/DynamoDB:
        
        - ELB tnajém tintégri wrah des RDS yé9blo mno request li bch yforwardiha ml client kima haka 
                                +----------+ 
                                | Client   |
                                +----------+
                                    | |
                                +-----------+
                                |    ELB    |
                                +-----------+
                                    | |
                        +---------+      +---------+
                        | RDS 1    | ... | RDS n    | (RDS/DynamoDB ou n'importe quel BD)
                        +---------+      +---------+
        
        En effet , ywaza3 les connections demandées 3al les BDs  bch yjéwb les requests kol w mati7 7ata BD 


    4. Integration with IAM:

                ┌─────────────────────┐
                │ Client              │
                └─────────────────────┘
                        │
                        ▼
                ┌─────────────────────┐
                │ IAM Service         |
                | ycontroli (Polcies  |
                | & Roles )           │
                └─────────────────────┘
                        │
                        ▼
                ┌─────────────────────┐
                │ Elastic Load        │
                │ Balancer (ELB)      │
                └─────────────────────┘
                        │
                        ▼
                ┌─────────────────────┐
                │ EC2 Instances       │
                └─────────────────────┘


        Client ylansi request bch yaccédi ll EC2 instances , hal request lézm tét3ada 3al ELB bch yforwadiha ll sérveur li ya5taro (algo + health) .. 
        Ama stana 9bal matousl ll ELB lézm tét3ada 3al bawéba ta3 IAM . IAM bch ycontroli l acces 3al ELB bl policies & roles (policies w roles en attente 
        na3mlou terraform ansible ta3 IAM .. nhbch blabla .. hano hot fmokhk li houma des règles wkhw ) . Ken client autorisé ykhalih yaccédi si nn yéb34o 
        wra chams . 


    5. Integration with Route 53:
        - ELB najmou nintégriwah (INDIRECTEMENT) m3a ELB kima haka : 
                        +-------------+ +-------------+        
                        | Route 53    | |   ELB 1...n | 
                        +-------------+ +-------------+
                                |           |
                            +---------------------+
                            |      Client         |
                            +---------------------+
        En effet , client yéjbd @ip ta3 ELB li lézm yémchilo mn 3and Route 53 (Serveur DNS ta3 AWS) w yab3a4lou request w ELB 
        mn ba3d yétsaraf fiha 

    6. Integration with VPC:
        - ELB généralement tal9ah m8atsino wost VPC (Privé/Public) .. ycommini m3a client mn chira w m3a srévér backend mn chira okhra 
        (Pas besoin de diagram t4akar juste exemple nana ta3 EC2)

    7. Integration with AWS Lambda 

      - ELB tnajém tintégri wrah des Lambda yé9blo mno request li bch yforwardiha ml client kima haka 
                                +----------+ 
                                | Client   |
                                +----------+
                                    | |
                                +-----------+
                                |    ELB    |
                                +-----------+
                                    | |
                        +---------+      +---------+
                        | Lambda 1| ..   | Lambda n|
                        +---------+      +---------+
        Une fois lambda 9bél request , ylansi code li fih , ygénéri response w yjéwb client ..
        -> Dans cet example lambda 3ibara API server .. (Serveur ydour fih code yétraiti les requests ta3 client 
        w yjéwb 3lihom ) 
        


    8. Integration with CloudWatch:
         
        ┌───────────────┐ ┌───────────────┐
        │ ELB           │ │ CloudWatch    │
        └───────┬───────┘ └───────┬───────┘
                │                 │
                │ CloudWatch      │
                │◀────────────────────────────┤
                                │ │
                                │ │
                                │ ├───────Logs───────────────────────▶│
                                │ │ │
                                │ ├─────Metrics────────────────────▶│
                                │ │ │
                                │ ├─────Alarms─────────────────────▶│
                                │ │ │
                                │ ├─────Events─────────────────────▶│
                                │ │ │
    

        Fl example:
            ELB ykhdm 3al rou7o , yé9bl les requests w yforwadi fihom , CloudWatch lésa9 fih ysajél fl les logs li ygénéri fihom ELB , w en se basant 3la 
            des Metrics inti tfaxihom ydéclenchi des alarmes , des events (tlansiyét code fl lambda par exemple etc .. ) 
            


    9. Integration with Auto Scaling:
        
            1. Diagram & Explication:
                Web App mdéployia fi des instances EC2 , les instances teb3in Auto Scaling Group , wé9éf 9odémhom ELB yforwardilhom fl requests 

                        ┌─────────────┐
                        │ ELB         │
                        └─────────────┘
                            │
                        ┌─────────────┐
                        │ Auto Scaling│
                        │ Group (ASG) │
                        └─────────────┘
                            │
                        ┌─────────────┐
                        │EC2 Instances 
                        └─────────────┘
                - Ndéfiniw des Policies fl ASG (9awé3éd une fois t7a9é9t ybadél fl infra ta3 EC2 li wrah .. CPU fét x% , Disk a9al mn x% etc .. ) 
                une fois wahda ml 9awé3d t7a9é9t yétlansa scaling event (ya3ml lmodif lézmfl EC2) .. les EC2 li tzédo par exemple yét9aydo 3and ELB 
                bch yda5alhom fi 7sébéto wa9t forwarding 


        10-11. Integration with CDN/CloudFormation: (Reporté)
            


10/ Advanced Features (Reporté Ulitérieurement nchalh) 
11/ module "elb" in Terraform : 

    module "elb" {
    # Inputs Obligatoires : 
        source  = "terraform-aws-modules/elb/aws//examples/complete"
        version = "4.0.1"

        subnets         = ["subnet-12345678", "subnet-87654321"] # IDs des subnets li bch tdeployi fihom el ELB 
        security_groups = ["subnet-12345678", "subnet-87654321"] # ID des SGs associés ll elb
        listener = [ # lista tsob fiha les requests kifh yforwardihom el ELB 
            {
            instance_port     = "80" #El port ta3 instance li bch yforwardilha el ELB el request
            instance_protocol = "http" # Protocol mosta3ml ml les instances (HTTP, HTTPS, TCP, SSL, etc.)
            lb_port           = "80" # El port ta3 ELB li bch yé9bl traffic 
            lb_protocol       = "http"  # El protocol eli yé9bl bih el ELB el traffic 
            },
            {
            instance_port     = "8080"
            instance_protocol = "http"
            lb_port           = "8080"
            lb_protocol       = "http"

            #            Note about SSL:
            #            This line is commented out because ACM certificate has to be "Active" (validated and verified by AWS, but Route53 zone used in this example is not real).
            #            To enable SSL in ELB: uncomment this line, set "wait_for_validation = true" in ACM module and make sure that instance_protocol and lb_protocol are https or ssl.
            #            ssl_certificate_id = "arn:aws:acm:eu-west-1:235367859451:certificate/6c270328-2cd5-4b2d-8dfd-ae8d0004ad31"
            }                           # Certificat SSL , lézm tfaxih ki tabda cnx HTTPS 
                                        # Certif SSL hétha généralement bch ykoun mkhazén fl ACM (AWS Certificate Manager) wili fl IAM .. 
        ]

        health_check = { # Voir RQ 1 
            target              = "HTTP:80/"
            interval            = 30
            healthy_threshold   = 2
            unhealthy_threshold = 2
            timeout             = 5
        }

    # Inputs Facultatifs : 
        name = "elb-example"
        internal        = false # ELB interne/externe (accessible kn ml bara wla ) 
        access_logs = { # ELB bch y5azén les logs ta3 les requests li yét3adéw 3lih w métadata mte3hom fl Bucket hétha 
            bucket = "my-access-logs-bucket"
        }

        # ELB attachments
        number_of_instances = 2 # 9adéh mn instance wra ELB
        instances           = ["i-06ff41a77dfb5349d", "i-4906ff41a77dfb53d"] # Les instances li bch yforwardilhom el ELB el traffic 

        tags = {
            Owner       = "user"
            Environment = "dev"
        }

        # Autres  
        cross_zone_load_balancing = false  # Voir RQ 2 
        idle_timeout = 60 # Voir RQ3 

    }


    RQ 1 : health_check : paramètre tconfiguri fih les health_check kifh lézm ysiro .. (health_check hia l'opération li ya3mlha
        ELB bch ydour 3ali wrah w ythabéthom cv w ynajémo yé9blo des requests wla )
            - `target`: Specifies the target for the health check. It defines the protocol, port, and path to check 
            the health of the instances. In the example, `"HTTP:80/"` indicates that the health check target is an 
            HTTP endpoint on port 80 with the path "/".

            - `interval`: 9adéh en secondes bin healthcheck w li ba3dou 

            - `timeout`: 9adéh yab9a lb yéstana fi response .. une fois b9at l'instance li yvérifi fiha x secondes > timeout

            - `healthy_threshold`: Une fois instance néj7ét healthy_threshold fois fl health test elle considéré healthy 

            - `unhealthy_threshold`: Une fois instance ta7ét healthy_threshold fois fl test elle considéré unhealthy 

    RQ 2 : cross_zone_load_balancing : Prenant le cas ou éno request fi AZ 9bélha ELB , hal paramètre bch yfixi est ce ELB 3ando 7a9 yforwadi request 
    l des instances fi des AZ okhrin (autre que AZ li fiha request ou pas) .. A noter eno si mis à true fault tolerance w availabilité akthr éma bch tfaktar 
    3lik flous akthr (transfert data bin les AZ yétkaléf) .. dc en gros ça dépend de l'archi 

    RQ 3 : idle_timeout : le temps en seconde li loadbalancer yab9a mkholi un connexion ma7loula m3ah w ma8ir mayjih 3an tri9ha données 
    .. Par exemple client 7al une cnx TCP w b3a4 3liha request .. loadbalancer ysakarha une fois majéto 3liha hata request 
    ba3d idle_timeout secondes .. lb yna4af rou7o bch lb mayébla3ch bl les cnx zéydin w yjéwb les cnx lokhrin mli7
   

12/ Outputs : (Reporté hata nchalah twali 3andi intuition aw4a7 3al outputs chma3néha ?  )
    # Lézm Rappel hna : .......... 

13/ Ansible Configuration : (Reporté en attente de révision Ansible inchalh)
  






   
  
3 : Terraform & Ansible : 
    # Question 1 : Chnowa far9 bin provisionnement bl module w bl resource 
    # Question 2 : Chnowa far9 bin provisionnement bl Terraform w bl Ansible 

# Questions : 
    1/ Modèle OSI 
    2/ chma3néha yopéri fl couche x .. ma3néha yésta3ml ses protocols ? 


