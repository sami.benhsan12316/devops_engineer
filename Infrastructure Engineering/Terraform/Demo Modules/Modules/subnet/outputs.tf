output "sub" {
    value = aws_subnet.myapp-subnet-1
}

output "rtb" {
    value = aws_route_table.myapp-route-table
}