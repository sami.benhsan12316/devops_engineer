provider "kubernetes" {
    load_config_file = "false" # manhbouch k8s yhabat config file 7a4ra w yhotha fl cluster .. bch nasn3o wahda jdida 
    hosts = data.aws_eks_cluster.myapp-cluster.endpoint # 3la ana serveur fi k8s cluster tsir l'authentif
    token = data.aws_eks_cluster_auth.myapp-cluster.token  # data li fiha les données lézmin bch ta3ml authentif  
    cluster_ca_certificat = base64decode(data.aws_eks_cluster.myapp-cluster.certificat_authority.0.data)
} 

data "aws_eks_cluster" "myapp-cluster" {
    name = module.eks.cluster_name  
    #Bch nqueriw les k8s clusters li sna3néhom fl AWS .. ay k8s cluster bl esm héka yokhroj 
} # Question! 14 

data "aws_eks_cluster_auth" "myapp-cluster" {  # yretourni object fih token .. 
    name = module.eks.cluster_name
}

module "eks" {
    source  = "terraform-aws-modules/eks/aws"
    version = "19.10.0"
v
    cluster_name = "myapp-cluster" # ATTENTION : L'esm li 7atou fl tags fl vpc .. mch mn 3andk 
    cluster_version = "1.17" # Version ta3 k8s .. hia faxétha mn mokhha 
    
    vpc_id = module.myapp-vpc.vpc_id 
    subnets = module.myapp-vpc.private_subnets # Voir RQ 9 + 10 .. Zid généralisation

    tags = {
        environnement = "development" 
        application = "myapp" 
    }

    worker_group = [ # Voir RQ 11 
        {
            instance_type = "t2.small" 
            name = "worker-group-1"
            asg_desired_capacity = 3 
        } , 
        {
            instance_type = "t2.medium" 
            name = "worker-group-2"
            asg_desired_capacity = 2 
        } 
    ]

}



