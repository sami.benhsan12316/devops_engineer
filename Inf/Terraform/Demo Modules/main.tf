resource "aws_vpc" "myapp-vpc-1" {
    cidr_block =  var.vpc_cidr_block
    tags = { 
        Name : "${var.env-prefix}-vpc"
    }
}

module "Subnet-Module" {
    source = "./Modules/subnet"
    vpc_id = aws_vpc.myapp-vpc-1.id
    subnet_cidr_block = var.subnet_cidr_block
    availability_zone = var.availability_zone
    env-prefix = var.env-prefix
}

resource "aws_route_table_association" "rtb-subnet" {
    subnet_id = module.subnet.sub.id 
    route_table_id =  module.subnet.rtb.id
}



resource "aws_security_group" "myapp-sg"  {
		name = "........" 
		vpc_id = aws_vpc.myvpc.id 
		ingress { 
			from_port = 22  
			to_port = 22 
			protocol = "tcp" 
			cidr_blocks = ["var.my_ip"]  
		}
		ingress {
			from_port = 8080 
			to_port  = 8080 
			protocol = "tcp" 
			cidr_blocks = ["0.0.0.0/0"]   
		}
		egress {
			from_port = 0  
			to_port = 0   
			protocol = "-1"	
			cidr_blocks =  ['0.0.0.0/0"]
			prefix_list_ids = []  
		}
	}


resource "aws_key..." "..." { # tgénéri clé publique w ba3d thot path te3ha hna
    key_pair =  ${file(var.path)}
}

data "aws_ami" "myapp-ami" {
    most_recent = true 
    owners = ['amazon']
    filters {
        name : "name"
        values : ['reg-exp']
    }
    filters {
        name : "virtualization-type" 
        values : ['hvm']
    }
}


resource "aws_instance" "myapp-instance" {
    ami = data.aws_ami.myapp-ami.id 
    instace_type = var.instance_type 

    subnet_id = module.subnet.sub.id 
	vpc_security_group_ids = [aws_security_group.my-app-security-group.id ] # Voir RQ 5
	availability_zone = var.availability_zone # Voir RQ 6 

	associate_public_ip_adress = true # Voir RQ 7 
	key_name = aws_key_pair.ssh-key.key_name 
	tags = {
		Name : "${var.env}-instance"
	}
}   
